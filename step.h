#ifndef STEP_H
#define STEP_H
#include "scale.h"

class Step{
public:
    Step();
    ~Step();
private:
    QString * description;
    Scale * scale;
};

#endif // STEP_H
