#include "mainwindow.h"
#include "databasemanager.h"
#include <QApplication>
#include <QMenu>
#include <QAction>
#include <QMenuBar>
#include <QDebug>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    dbm = new DatabaseManager();
    dbm->createTables();
    setWindowTitle(tr("LAVSLIBERORVM"));
    setMinimumSize(160, 160);
    resize(480, 320);
    createActions();
    createMenus();

}

MainWindow::~MainWindow()
{
    delete dbm;
    delete exitAct;
    delete fileMenu;
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(createAct);
    fileMenu->addAction(exitAct);
}

void MainWindow::createActions()
{

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &QMainWindow::close);

    createAct = new QAction(tr("Create &Groupfile"), this);
    createAct->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_N));
    createAct-> setStatusTip(tr("Create a new Groupfile"));
    connect(createAct, &QAction::triggered, this, &MainWindow::createFile);
}

void MainWindow::createFile(){
    qDebug() << "new clicked" << Qt::endl;
}
