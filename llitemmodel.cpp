#include "llitemmodel.h"

LLItemModel::LLItemModel(QObject *parent)
    : QAbstractItemModel(parent)
{// set up sq to load stuff
    this->dataList = new QList<DayData>;
}

LLItemModel::~LLItemModel()
{
    delete[] dataList;
}

QModelIndex LLItemModel::index(int row, int column, const QModelIndex &parent) const
{
    // FIXME: Implement me!
    //in simle trtee model ist ein Beispiel, wie das gehen könnte. Es hat wohl doch mehr mit QItemIndex auf sich, von wegen interne Pointer
    //QModelIndex * p = &parent;
    //return QAbstractItemModel::createIndex(row, column, p);
}

QModelIndex LLItemModel::parent(const QModelIndex &index) const
{
    // FIXME: Implement me!
}

int LLItemModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

int LLItemModel::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

bool LLItemModel::hasChildren(const QModelIndex &parent) const
{
    // FIXME: Implement me!
}

bool LLItemModel::canFetchMore(const QModelIndex &parent) const
{
    // FIXME: Implement me!
    return false;
}

void LLItemModel::fetchMore(const QModelIndex &parent)
{
    // FIXME: Implement me!
}

QVariant LLItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}

bool LLItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags LLItemModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool LLItemModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
}

bool LLItemModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endInsertColumns();
}
