#include "databasemanager.h"

#include <QRandomGenerator>

DatabaseManager::DatabaseManager() : DatabaseManager(QDir::currentPath()+"/database.db")
{
}



DatabaseManager::DatabaseManager(QString db_dir)
{
    //create database object
    db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    //set path for database
    QFileInfo *fileInfo = new QFileInfo(db_dir);
    dbFileExistant = fileInfo->exists();
    //setting the name of the database sets the filename for Sqlite databases
    db->setDatabaseName(db_dir);
    //open the database
    if (!db->open()){
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specigic error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        // TODO throw exception
    }
    //enable foreign keys
    QSqlQuery query;
    if (!query.exec("PRAGMA foreign_keys = ON;")){
        qDebug() << "Error as reported by Database: " << query.lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << query.lastError().driverText() << Qt::endl;
        qDebug() << "Database specigic error code: " << query.lastError().nativeErrorCode() << Qt::endl;
        // TODO throw exception
    }
}


bool DatabaseManager::createTables()
{
    if (!dbFileExistant){
        db->transaction();
        QSqlQuery query;

        // creating the actual tables
        query.exec("create table tbl_name ("
                        "name_id integer not null primary key autoincrement, "
                        "name nvarchar unique not null)");

        query.exec("create table tbl_day ("
                        "day_id integer not null primary key autoincrement, "
                        "day date not null, "
                        "name_id integer not null, "
                        "description nvarchar, "
                            "foreign key (name_id) references tbl_name (name_id))");

        query.exec("create table tbl_step ("
                        "step_id integer not null primary key autoincrement, "
                        "day_id integer not null, "
                        "definition_id integer not null, "
                            "foreign key (day_id)        references tbl_day              (day_id), "
                            "foreign key (definition_id) references tbl_step_definitions (definition_id))");

        query.exec("create table tbl_step_definitions ("
                        "definition_id integer not null primary key autoincrement, "
                        "description nvarchar unique not null)");

        query.exec("create table tbl_stepscales ("
                        "stepscale_id integer not null primary key autoincrement, "
                        "step_id integer not null, "
                        "scale_id integer not null, "
                            "foreign key (step_id)      references tbl_step            (step_id), "
                            "foreign key (scale_id)     references tbl_scale           (scale_id)"
                    "unique(step_id))");

        query.exec("create table tbl_scale ("
                        "scale_id integer not null primary key autoincrement, "
                        "name nvarchar unique not null)");

        query.exec("create table tbl_measure ("
                        "measure_id integer not null primary key autoincrement, "
                        "scale_id integer not null, "
                        "description nvarchar not null, "
                        "value integer not null, "
                            "foreign key (scale_id) references tbl_scale (scale_id))");

        query.exec("create table tbl_laudes ("
                        "stepscale_id integer not null, "
                        "measure_id integer not null, "
                   "foreign key (stepscale_id) references tbl_stepscales (stepscale_id), "
                   "foreign key (measure_id) references tbl_measure (measure_id)"
                   "unique(stepscale_id))");

        query.exec("create table tbl_definitions_hierarchy ("
                        "parent integer not null, "
                        "child integer not null primary key, "
                            "foreign key (parent) references tbl_step_definitions (definition_id), "
                            "foreign key (child)  references tbl_step_definitions (definition_id))");
        if (!db->commit()){
            db->rollback();
            qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
            qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
            qDebug() << "Database specigic error code: " << db->lastError().nativeErrorCode() << Qt::endl;
            return false;
        }

        // fill tables with standard data

        // tbl_scale
        // setting data
        QVector<std::tuple<QString, int>> measureData;
        QVector<QString> mdescriptions;
        mdescriptions << "nicht erreicht"<<"zum Teil erreicht"<<"weitgehend erreicht"<<"erreicht";
        for (int i = 0; i < mdescriptions.size(); i++){
            measureData.append(std::make_tuple(mdescriptions[i], i));
            // measureData.append(QPair<QString, int>(mdescriptions[i], i));
        }
        // applying to database
        if(!addScale("standard", measureData)){
            qDebug() << "Error: Scale could not be added with measureData = ";
            for (int i = 0; i < measureData.size(); i++){
                qDebug() << std::get<0>(measureData[i]) << " | " << std::get<1>(measureData[i]);
            }
            return false;
        }


        // tbl_name
        QVector<QString> name;
        name << "Heinz" << "Erna";
        for (int i = 0; i< name.size();i++){
            if(!addName(name[i])){
                qDebug() << "Error: Name could not be added with name = " << name[i];
                return false;
            }
        }


        // tbl_day
        addDay(
               QDate::fromString("2020-01-01", Qt::ISODate),
               getNameId("Heinz"),
               "Heinz hat verschlafen wegen der langen Party."
        );
        addDay(
               QDate::fromString("2020-01-02", Qt::ISODate),
               getNameId("Heinz"),
               "Heinz hat beschlossen, nie wieder Alkohol zu trinken."
        );
        addDay(
               QDate::fromString("2020-01-01", Qt::ISODate),
               getNameId("Erna"),
               "Erna hat heute Rücksicht gezeigt und Heinz schlafen lassen."
        );
        addDay(
               QDate::fromString("2020-01-02", Qt::ISODate),
               getNameId("Erna"),
               "Erna ärgert wie zuvor."
        );


        // tbl_step_definitions & tbl_definitions_hierarchy
        for (int i = 0; i < 6; i++){
            QString oberschritt = "Oberschritt " + QString::number(i+1);
            if (!addStepDefinition(oberschritt)){
                qDebug() << "Error: Oberschritt could not be added with Oberschritt = " << oberschritt;
                return false;
            }
            for (int j=0; j < 3; j++){
                 QString unterschritt = "Unterschritt "+QString::number(i+1)+"."+QString::number(j+1);
                 if (!addStepDefinition(unterschritt, oberschritt)){
                     qDebug() << "Error: Unterschritt could not be added with Unterschritt = " << unterschritt
                              << " and Oberschritt = " << oberschritt;
                     return false;
                 }
            }
        }

        // tbl_step
        // iterate over days: 1..3
        for (int day = 1; day < 4; day = day + 2){
            // iterate over definitions. There are 16
            for (int def = 1; def <= 16; def++ ){
                int day2;
                // if the definition id is higher than 8 it belongs to the 2md day, so increase day 1->2, 3->4
                def > 8 ? day2 = day + 1 : day2 = day;
                addStep( day2, def );
            }
        }

        // tbl_stepscales
        query.exec("SELECT COUNT(step_id) as amount FROM tbl_step");
        query.first();
        int amount = query.value("amount").toInt();
        for (int i=1; i <= amount; i++){
            // every third step has no scale being the Oberschritt
            if ((i-1) %4 != 0){
                addStepScale(i,1);
            }
        }

        // tbl_laudes
        QRandomGenerator qrg;
        for (int i = 1; i < 25; i++){
            if (!addLaud(i, qrg.bounded(4)+1)){
            }
        }

    }
    return true;
}

DatabaseManager::~DatabaseManager()
{
    db->close();
}

// adders -------------------------------------------------------------------------------------------------------

// tbl_scale & tbl_measure
bool DatabaseManager::addScale(QString scaleName, QVector<std::tuple<QString, int> > measureData)
{ // signatur

    QSqlQuery query;

    // start transaction
    db->transaction();

    // tbl_scale
    query.prepare("INSERT INTO tbl_SCALE (name) VALUES (:scaleName)");
    query.bindValue(":scaleName", scaleName);
    query.exec();

    //tbl_measure
    int scale_id = query.lastInsertId().toInt();
    query.prepare("INSERT INTO tbl_MEASURE "
               "(scale_id, description, value) "
               "VALUES (:scale_id, :desc, :val)");
    for (int i = 0; i < measureData.size(); ++i) {
        query.bindValue(":scale_id", scale_id);
        query.bindValue(":desc", std::get<0>(measureData[i]));
        query.bindValue(":val", std::get<1>(measureData[i]));
        query.exec();
    }
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addName(QString name)
{
    QSqlQuery query;

    // start transaction
    db->transaction();
    query.prepare("INSERT INTO tbl_name (name) VALUES (:name)");
    query.bindValue(":name", name);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addDay(QDate date, int name_id, QString desc)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("INSERT INTO tbl_day (day, name_id, description) VALUES (:date, :name_id, :desc)");
    query.bindValue(":date", date.toString(Qt::ISODate));
    query.bindValue(":name_id", name_id);
    query.bindValue(":desc", desc);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addStepDefinition(QString desc, QString parentDesc)
{
        return addStepDefinition( desc, getStepDefinition(parentDesc) );
}

bool DatabaseManager::addStepDefinition(QString desc, int parentId)
{
    QSqlQuery query;
    if (!addStepDefinition(desc)){
        qDebug() << "Error while adding StepDefinition "<< desc <<" to Database";
        return false;
    }
    int childId = getStepDefinition(desc);
    db->transaction();
    query.prepare("INSERT INTO tbl_definitions_hierarchy (child, parent) VALUES (:childId, :parentId)");
    query.bindValue(":childId", childId);
    query.bindValue(":parentId", parentId);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }

    return true;
}

bool DatabaseManager::addStepDefinition(QString desc)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("INSERT INTO tbl_step_definitions (description) VALUES (:desc)");
    query.bindValue(":desc", desc);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addStep(int day_id, int definition_id)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("INSERT INTO tbl_step (day_id, definition_id) VALUES (:day_id, :definition_id)");
    query.bindValue(":day_id", day_id);
    query.bindValue(":definition_id", definition_id);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addStepScale(int step_id, int scale_id)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("INSERT INTO tbl_stepscales (step_id, scale_id) VALUES (:step_id, :scale_id)");
    query.bindValue(":step_id", step_id);
    query.bindValue(":scale_id", scale_id);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}

bool DatabaseManager::addLaud(int stepscale_id, int measure_id)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("INSERT INTO tbl_laudes (stepscale_id, measure_id) VALUES (:stepscale_id, :measure_id)");
    query.bindValue(":stepscale_id", stepscale_id);
    query.bindValue(":measure_id", measure_id);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return false;
    }
    return true;
}


// getters --------------------------------------------------------------------------------------------------------

int DatabaseManager::getNameId(QString name)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("SELECT name_id from tbl_name where name = :name;");
    query.bindValue(":name", name);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return -1;
    }
    query.first();
    return query.value("name_id").toInt();
}

int DatabaseManager::getStepDefinition(QString desc)
{
    QSqlQuery query;
    db->transaction();
    query.prepare("SELECT definition_id FROM tbl_step_definitions WHERE description = :pDesc");
    query.bindValue(":pDesc", desc);
    query.exec();
    if (!db->commit()){
        db->rollback();
        qDebug() << "Error as reported by Database: " << db->lastError().databaseText() << Qt::endl;
        qDebug() << "Error as reported by Database Driver: " << db->lastError().driverText() << Qt::endl;
        qDebug() << "Database specific error code: " << db->lastError().nativeErrorCode() << Qt::endl;
        return -1;
    }
    query.first();
    return query.value("definition_id").toInt();
}












