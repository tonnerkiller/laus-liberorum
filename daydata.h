#ifndef DAYDATA_H
#define DAYDATA_H

#include <QDate>
#include <QList>
#include "step.h"

class QDate;

class DayData
{
public:
    DayData();
    ~DayData();
private:
    QString * name;
    QDate * date;
    QString * dayNote;
    QList<Step> * steps;
};

#endif // DAYDATA_H
