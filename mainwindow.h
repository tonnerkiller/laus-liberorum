#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QSqlDatabase>
#include "databasemanager.h"
//forward declarations
class QPushButton;
class QLineEdit;
class QSqlDatabase;
//class DBManager;
class QMenu;
class QAction;

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    DatabaseManager * dbm;


private:


//Menu
    QMenu *fileMenu;
    QAction *exitAct;
    QAction *createAct;
    void createActions();
    void createMenus();

private slots:
    void createFile();

};
#endif // MAINWINDOW_H
