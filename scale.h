#ifndef SCALE_H
#define SCALE_H
#include "measure.h"

class Scale{
public:
    Scale();
    ~Scale();
private:

    QString * name;
    Measure * measure;
};

#endif // SCALE_H
