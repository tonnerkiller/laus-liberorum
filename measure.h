#ifndef MEASURE_H
#define MEASURE_H

// forward declarations
class QString;


// the actual class
class Measure{
public:
    Measure();
    ~Measure();
private:
    QString * description;
    int * value;
    bool * selected;
};
#endif // MEASURE_H
