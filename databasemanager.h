#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QSqlDatabase>
#include <QDir>
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QDate>
#include <tuple>


class DatabaseManager
{
public:
    DatabaseManager(QString d);
    DatabaseManager();
    bool createTables();
    ~DatabaseManager();
    bool addScale(QString scaleName, QVector<std::tuple<QString, int>> measureData);
    bool addName(QString name);
    bool addDay(QDate date, int name_id, QString desc);
    bool addStepDefinition(QString desc, QString parentDesc);
    bool addStepDefinition(QString desc, int parentId);
    bool addStepDefinition(QString desc);
    bool addStep(int day_id, int definition_id);
    bool addStepScale(int step_id, int scale_id);
    bool addLaud(int stepscale_id, int measure_id);
    int getNameId(QString name);
    int getStepDefinition(QString desc);
private:
    QSqlDatabase * db;
    bool dbFileExistant;
};

#endif // DATABASEMANAGER_H
